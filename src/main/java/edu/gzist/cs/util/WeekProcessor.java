package edu.gzist.cs.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WeekProcessor {
    public static void main(String[] args) {
        String input = "(10-10节)1-2周,4-6(单)周,7-10(双)周,12-13周,14周";
        int[] result1 = extractNumbersFromParentheses(input);
        List<Integer> extractedNumbers = extractAllNumbers(input);

        // 输出结果
        System.out.println("Extracted numbers: " + extractedNumbers);
        System.out.println("Extracted numbers from parentheses: " + result1[0] + ", " + result1[1]);

    }

    /**
     * 从给定的字符串中提取形如 (a-b节) 的部分，并返回 a 和 b 作为整数数组。
     *
     * @param str 输入的字符串
     * @return 包含两个整数的数组
     */
    private static int[] extractNumbersFromParentheses(String str) {
        String regex = "\\((\\d+)-(\\d+)节\\)";
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(regex);
        java.util.regex.Matcher matcher = pattern.matcher(str);

        if (matcher.find()) {
            int num1 = Integer.parseInt(matcher.group(1));
            int num2 = Integer.parseInt(matcher.group(2));
            return new int[]{num1, num2};
        } else {
            throw new IllegalArgumentException("Input string does not match the expected format.");
        }
    }


    /**
     * 从给定的字符串中提取所有形如 c-d周、c-d(单)周、c-d(双)周 或 c周 的部分，并返回所有数字组成的列表。
     *
     * @param str 输入的字符串
     * @return 包含所有提取到的整数的列表
     */
    private static List<Integer> extractAllNumbers(String str) {
        List<Integer> numbers = new ArrayList<>();
        String regex = "(,?\\d+)周|(,?\\d+)-(\\d+)周|(,?\\d+)-(\\d+)\\((单|双)\\)周"; // 正则表达式
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        while (matcher.find()) {
            if (matcher.group(1) != null) {
                // 匹配到单独的数字 ,c周
                int num = Integer.parseInt(matcher.group(1).replace(",", ""));
                numbers.add(num);
            } else if (matcher.group(2) != null && matcher.group(3) != null) {
                // 匹配到范围 ,c-d周
                int num1 = Integer.parseInt(matcher.group(2).replace(",", ""));
                int num2 = Integer.parseInt(matcher.group(3));
                numbers.add(num1);
                numbers.add(num2);
            } else if (matcher.group(4) != null && matcher.group(5) != null) {
                // 匹配到范围 ,c-d(单)周 或 ,c-d(双)周
                int num1 = Integer.parseInt(matcher.group(4).replace(",", ""));
                int num2 = Integer.parseInt(matcher.group(5));
                numbers.add(num1);
                numbers.add(num2);
            }
        }

        return numbers;
    }
}


